import { showModal } from './modal';

export function showWinnerModal(fighter) {

  const modalObj = {
    title: `${fighter.name} is win!`,
    bodyElement: 'Молодец!!!!'
  }

  showModal(modalObj);
}
