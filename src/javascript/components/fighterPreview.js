import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)

  if (fighter) {
    const fighterImg = createFighterImage(fighter);

    const fighterName = createElement({
      tagName: 'h2',
      className: `fighter-preview__name`,
    });
    fighterName.innerHTML = fighter.name;

    const fighterInfo = createElement({
      tagName: 'p',
      className: `fighter-preview__info`,
    });
    fighterInfo.append(`Health: ${fighter.health} / Attack: ${fighter.attack} / Defense: ${fighter.defense}`);

    fighterElement.append(fighterName, fighterInfo, fighterImg);
  }

  // const fighterImg = createFighterImage(fighter);
  // fighterElement.append(fighterImg);

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
