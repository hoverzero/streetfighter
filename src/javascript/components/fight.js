import { controls } from '../../constants/controls';
import { showWinnerModal } from './modal/winner';

let winner;

export async function fight(firstFighter, secondFighter) {

  firstFighter.healthBar = document.getElementById('left-fighter-indicator');
  secondFighter.healthBar = document.getElementById('right-fighter-indicator');

  firstFighter.currentHealth = firstFighter.health;
  secondFighter.currentHealth = secondFighter.health;

  let isFirstFighterBlock = false;
  let isSecondFighterBlock = false;
  
  let playerOneCombo = [];
  let playerTwoCombo = [];

  let isPlayerOneCombo = true;
  let isPlayerTwoCombo = true;

  document.addEventListener('keydown', event => {

    switch(event.code) {
      case controls.PlayerOneAttack:
        if (!isFirstFighterBlock) {
          setSimpleDamage(firstFighter, secondFighter, isSecondFighterBlock);
        }
        break;
      case controls.PlayerOneBlock:
        isFirstFighterBlock = true;
        break;
      case controls.PlayerTwoAttack:
        if (!isSecondFighterBlock) {
          setSimpleDamage(secondFighter, firstFighter, isFirstFighterBlock);
        }
        break;
      case controls.PlayerTwoBlock:
        isSecondFighterBlock = true;
        break;
    }

    if (controls.PlayerOneCriticalHitCombination.includes(event.code) && !playerOneCombo.includes(event.code)) {
      playerOneCombo.push(event.code);
    }else if (controls.PlayerTwoCriticalHitCombination.includes(event.code) && !playerTwoCombo.includes(event.code)) {
      playerTwoCombo.push(event.code);
    }

    if (playerOneCombo.length === 3 && isPlayerOneCombo) {
      setCriticalDamage(firstFighter, secondFighter);
      isPlayerOneCombo = false;
      setTimeout(() => {isPlayerOneCombo = true}, 10000);
    }else if (playerTwoCombo.length === 3 && isPlayerTwoCombo) {
      setCriticalDamage(secondFighter, firstFighter);
      isPlayerTwoCombo = false;
      setTimeout(() => {isPlayerTwoCombo = true}, 10000);
    }

    if (winner) {
      showWinnerModal(winner);
    }

  })

  document.addEventListener('keyup', event => {
    if (event.code === controls.PlayerOneBlock) {
      isFirstFighterBlock = false;
    }else if (event.code === controls.PlayerTwoBlock) {
      isSecondFighterBlock = false;
    }else if (playerOneCombo.includes(event.code)) {
      playerOneCombo.splice(playerOneCombo.indexOf(event.code), 1);
    }else if (playerTwoCombo.includes(event.code)) {
      playerTwoCombo.splice(playerTwoCombo.indexOf(event.code), 1);
    }
  })

  // return new Promise((resolve) => {
  //   resolve the promise with the winner when fight is over
  // });

}

export function getDamage(attacker, defender, isBlock) {

  if (!isBlock) {
    return getHitPower(attacker);
  }else {

    let hitPower = getHitPower(attacker);
    let blockPower = getBlockPower(defender);

    if (hitPower <= blockPower) {
      let damage = 0;
      return damage;
    }

    let damage = hitPower - blockPower;
    return damage;

  } 

}

export function getHitPower(fighter) {
  return randomInteger(1, 2, fighter.attack);
}

export function getBlockPower(fighter) {
  return randomInteger(1, 2, fighter.defense);
}

export function getCriticalDamage(attacker) {
  let power = attacker.attack * 2;
  return power;
}

export function setSimpleDamage(attacker, defender, isBlock) {
  defender.currentHealth -= getDamage(attacker, defender, isBlock);
  setHealthBar(attacker, defender);
}

export function setCriticalDamage(attacker, defender) {
  defender.currentHealth -= getCriticalDamage(attacker);
  setHealthBar(attacker, defender);
}

export function setHealthBar(attacker, defender) {
  const currentHealthBar = defender.currentHealth * 100 / defender.health;

  if (currentHealthBar <= 0) {
    defender.healthBar.style.width = 0;
    setWinner(attacker);
    return;
  }

  defender.healthBar.style.width = currentHealthBar + '%';
  if (currentHealthBar <= 50 && currentHealthBar > 20) {
    defender.healthBar.style.backgroundColor = '#db9a01';
  }else if (currentHealthBar <= 20) {
    defender.healthBar.style.backgroundColor = '#ff0000';
  }
}

export function setWinner(fighter) {
  winner = fighter;
}

function randomInteger(min, max, num) {
  let rand = min + Math.random() * (max + 1 - min);
  return Math.floor(rand) * num;
}
